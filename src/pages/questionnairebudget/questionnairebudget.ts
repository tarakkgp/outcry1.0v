import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnairebudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnairebudget',
  templateUrl: 'questionnairebudget.html',
})
export class QuestionnairebudgetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnairebudgetPage');
  }

  questionnaireDate() {
    this.navCtrl.push('QuestionnairedatePage');
  }

  goQuestionnaireEventDuration() {
    this.navCtrl.push('QuestionnaireeventdurationPage');
  }

}
