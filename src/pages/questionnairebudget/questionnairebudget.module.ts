import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnairebudgetPage } from './questionnairebudget';

@NgModule({
  declarations: [
    QuestionnairebudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnairebudgetPage),
  ],
})
export class QuestionnairebudgetPageModule {}
