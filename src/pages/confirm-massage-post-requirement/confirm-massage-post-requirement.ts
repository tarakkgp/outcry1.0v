import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfirmMassagePostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-massage-post-requirement',
  templateUrl: 'confirm-massage-post-requirement.html',
})
export class ConfirmMassagePostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmMassagePostRequirementPage');
  }


  cancelRequestPostRequirement() {
    this.navCtrl.push('CancelRequestPostRequirementPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }

  goChatdetails() {
    this.navCtrl.push('Chat');
  }

  goSchedulecallback() {
    this.navCtrl.push('ScheduleCallBackPage');
  }
}
