import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmMassagePostRequirementPage } from './confirm-massage-post-requirement';

@NgModule({
  declarations: [
    ConfirmMassagePostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmMassagePostRequirementPage),
  ],
})
export class ConfirmMassagePostRequirementPageModule {}
