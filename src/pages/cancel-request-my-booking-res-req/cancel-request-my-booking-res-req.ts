import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CancelRequestMyBookingResReqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-request-my-booking-res-req',
  templateUrl: 'cancel-request-my-booking-res-req.html',
})
export class CancelRequestMyBookingResReqPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelRequestMyBookingResReqPage');
  }

  myBookingNotResDetailedMsg() {
    this.navCtrl.push('MyBookingNotRespondedDetailedMsgPage');
  }
 
  goMyBookingRequirementDetail() {
    this.navCtrl.push('MyBookingRequirementDetailPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }

}
