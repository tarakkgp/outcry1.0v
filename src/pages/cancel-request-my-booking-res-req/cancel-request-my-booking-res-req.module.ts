import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelRequestMyBookingResReqPage } from './cancel-request-my-booking-res-req';

@NgModule({
  declarations: [
    CancelRequestMyBookingResReqPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelRequestMyBookingResReqPage),
  ],
})
export class CancelRequestMyBookingResReqPageModule {}
