import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ContactOutcrySupportTeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-outcry-support-team',
  templateUrl: 'contact-outcry-support-team.html',
})
export class ContactOutcrySupportTeamPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactOutcrySupportTeamPage');
  }

  goHelpCentreListing() {
    this.navCtrl.push('HelpCentreListingPage');
  }

}
