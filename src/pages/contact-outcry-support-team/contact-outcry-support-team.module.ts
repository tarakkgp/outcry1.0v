import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactOutcrySupportTeamPage } from './contact-outcry-support-team';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ContactOutcrySupportTeamPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactOutcrySupportTeamPage),
    ComponentsModule
  ],
})
export class ContactOutcrySupportTeamPageModule {}
