import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HelpCentreListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help-centre-listing',
  templateUrl: 'help-centre-listing.html',
})
export class HelpCentreListingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpCentreListingPage');
  }

  contactOutcrySupportTeam() {
    this.navCtrl.push('ContactOutcrySupportTeamPage');
  }

  scheduleCallBack() {
    this.navCtrl.push('ScheduleCallBackPage');
  }

  faqListingPage() {
    this.navCtrl.push('FaqListPage');
  }

  goProfile() {
    this.navCtrl.push('Profile');
  }
}