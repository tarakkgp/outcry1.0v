import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpCentreListingPage } from './help-centre-listing';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    HelpCentreListingPage,
  ],
  imports: [
    IonicPageModule.forChild(HelpCentreListingPage),
    ComponentsModule
  ],
})
export class HelpCentreListingPageModule {}
