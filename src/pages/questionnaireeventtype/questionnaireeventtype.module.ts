import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireeventtypePage } from './questionnaireeventtype';

@NgModule({
  declarations: [
    QuestionnaireeventtypePage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireeventtypePage),
  ],
})
export class QuestionnaireeventtypePageModule {}
