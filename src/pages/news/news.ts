import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SuperTabsController } from 'ionic2-super-tabs';
 
@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  rootNavCtrl: NavController;
 
  constructor(public navCtrl: NavController, public navParams: NavParams, private superTabsCtrl: SuperTabsController) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
  }
 
  goToDetails() {
    this.rootNavCtrl.push('NewsDetailsPage');
  }
 
  setBadge() {
    this.superTabsCtrl.setBadge('aboutTab', 9);
  }
 
  clearBadge() {
    this.superTabsCtrl.clearBadge('aboutTab');
  }
 
  jumpToAccount() {
    this.superTabsCtrl.slideTo(2);
  }
 
  hideToolbar() {
    this.superTabsCtrl.showToolbar(false);
  }
 
  showToolbar() {
    this.superTabsCtrl.showToolbar(true);
  }
}