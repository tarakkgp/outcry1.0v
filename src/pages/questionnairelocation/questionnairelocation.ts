import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnairelocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnairelocation',
  templateUrl: 'questionnairelocation.html',
})
export class QuestionnairelocationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnairelocationPage');
  }

  cancelRequest() {
    this.navCtrl.push('CancelRequestPage');
  }

  confirmMassageBookingProfessional() {
    this.navCtrl.push('ConfirmMassageBookingProfessionalPage');
  }

  goQuestionnaireDate() {
    this.navCtrl.push('QuestionnairedatePage');
  }
  
  goQuestionnairedate() {
    this.navCtrl.push('QuestionnairedatePage');
  }

  goQuestionaireForOtherQuestion() {
    this.navCtrl.push('QuestionaireForOtherQuestionPage');
  }
}
