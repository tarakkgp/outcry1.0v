import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnairelocationPage } from './questionnairelocation';

@NgModule({
  declarations: [
    QuestionnairelocationPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnairelocationPage),
  ],
})
export class QuestionnairelocationPageModule {}
