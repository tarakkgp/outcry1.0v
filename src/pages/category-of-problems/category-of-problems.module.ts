import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryOfProblemsPage } from './category-of-problems';

@NgModule({
  declarations: [
    CategoryOfProblemsPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryOfProblemsPage),
  ],
})
export class CategoryOfProblemsPageModule {}
