import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleCallBackPage } from './schedule-call-back';
import { ComponentsModule } from '../../components/components.module';
import { CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';

@NgModule({
  declarations: [
    ScheduleCallBackPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleCallBackPage),
    ComponentsModule,

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class ScheduleCallBackPageModule {}
