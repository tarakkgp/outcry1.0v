import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ScheduleCallBackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule-call-back',
  templateUrl: 'schedule-call-back.html',
})
export class ScheduleCallBackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduleCallBackPage');
  }

  goHelpCentreListing() {
    this.navCtrl.push('HelpCentreListingPage');
  }


}
