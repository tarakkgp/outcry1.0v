import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireEventTypePostRequirementPage } from './questionnaire-event-type-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireEventTypePostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireEventTypePostRequirementPage),
  ],
})
export class QuestionnaireEventTypePostRequirementPageModule {}
