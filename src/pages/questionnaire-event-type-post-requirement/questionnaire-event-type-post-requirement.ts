import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireEventTypePostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-event-type-post-requirement',
  templateUrl: 'questionnaire-event-type-post-requirement.html',
})
export class QuestionnaireEventTypePostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireEventTypePostRequirementPage');
  }

  questionnaireEventDurationPostRequirement() {
    this.navCtrl.push('QuestionnaireEventDurationPostRequirementPage');
  }

  goQuestionnaireStartingPagePostRequirement() {
    this.navCtrl.push('QuestionnaireStartingPagePostRequirementPage');
  }

}
