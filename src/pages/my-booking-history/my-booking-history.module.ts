import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingHistoryPage } from './my-booking-history';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyBookingHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingHistoryPage),
    ComponentsModule
  ],
})
export class MyBookingHistoryPageModule {}
