import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-my-booking-history',
  templateUrl: 'my-booking-history.html',
})
export class MyBookingHistoryPage {
  tab:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingHistoryPage');
    this.tab = "active";
  }

  tab_swap(type){
    this.tab = type;
  }
  
  projectlist() {
  this.navCtrl.push('Projectlist');
  }

}
