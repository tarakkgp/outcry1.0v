import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireDatePostRequirementPage } from './questionnaire-date-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireDatePostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireDatePostRequirementPage),
  ],
})
export class QuestionnaireDatePostRequirementPageModule {}
