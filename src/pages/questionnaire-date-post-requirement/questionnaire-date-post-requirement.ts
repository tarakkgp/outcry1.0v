import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireDatePostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-date-post-requirement',
  templateUrl: 'questionnaire-date-post-requirement.html',
})
export class QuestionnaireDatePostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireDatePostRequirementPage');
  }

  questionnaireLocationPostRequirement() {
    this.navCtrl.push('QuestionnaireLocationPostRequirementPage');
  }

  goQuestionnaireBudgetPostRequirement() {
    this.navCtrl.push('QuestionnaireBudgetPostRequirementPage');
  }
}
