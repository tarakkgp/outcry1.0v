import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CancelRequestPostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-request-post-requirement',
  templateUrl: 'cancel-request-post-requirement.html',
})
export class CancelRequestPostRequirementPage {



  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelRequestPostRequirementPage');
  }

  myBookingOngoingListing() {
    this.navCtrl.push('MyBookingOngoingListingPage');
  }

  myBookingHistory() {
    this.navCtrl.push('MyBookingHistoryPage');
  }

  goConfirmMassagePostRequirement() {
    this.navCtrl.push('ConfirmMassagePostRequirementPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }
}
