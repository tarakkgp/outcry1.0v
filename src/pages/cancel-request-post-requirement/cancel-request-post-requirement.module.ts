import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelRequestPostRequirementPage } from './cancel-request-post-requirement';

@NgModule({
  declarations: [
    CancelRequestPostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelRequestPostRequirementPage),
  ],
})
export class CancelRequestPostRequirementPageModule {}
