import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageListingPage } from './message-listing';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    MessageListingPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageListingPage),
    ComponentsModule,
  ],
})
export class MessageListingPageModule {}
