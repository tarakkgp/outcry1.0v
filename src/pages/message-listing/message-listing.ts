import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController,ModalController,Content, NavParams } from 'ionic-angular';

/**
 * Generated class for the MessageListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message-listing',
  templateUrl: 'message-listing.html',
})
export class MessageListingPage {
  @ViewChild(Content) content: Content;
 scrollPosition: number = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageListingPage');
  }

  profile(){
    this.navCtrl.push('Profile');
  }

  gochat() {
    this.navCtrl.push('Chat');
  }
}
