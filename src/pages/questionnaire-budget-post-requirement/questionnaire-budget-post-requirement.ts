import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireBudgetPostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-budget-post-requirement',
  templateUrl: 'questionnaire-budget-post-requirement.html',
})
export class QuestionnaireBudgetPostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireBudgetPostRequirementPage');
  }


  questionnaireDatePostRequirement() {
    this.navCtrl.push('QuestionnaireDatePostRequirementPage');
  }

  goQuestionnaireEventDurationPostRequirement() {
    this.navCtrl.push('QuestionnaireEventDurationPostRequirementPage');
  }

}
