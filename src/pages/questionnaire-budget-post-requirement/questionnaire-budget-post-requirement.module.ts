import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireBudgetPostRequirementPage } from './questionnaire-budget-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireBudgetPostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireBudgetPostRequirementPage),
  ],
})
export class QuestionnaireBudgetPostRequirementPageModule {}
