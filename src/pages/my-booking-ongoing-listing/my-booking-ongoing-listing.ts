import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,Content, PopoverController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';


/**
 * Generated class for the MyBookingOngoingListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-booking-ongoing-listing',
  templateUrl: 'my-booking-ongoing-listing.html',
})
export class MyBookingOngoingListingPage {
  tab:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertController: AlertController,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingOngoingListingPage');
    this.tab = "active";
  }

  helpPopover() {
    let popover = this.popoverCtrl.create('MybookingGetHelpPage');
    popover.present();
  }

  tab_swap(type){
    this.tab = type;
  }
  
  projectlist() {
  this.navCtrl.push('Projectlist');
  }

  async help() {
    const alert = await this.alertController.create({
      title: 'Schedule Call Back',
      subTitle:'</hr>',
      message: 'Please Schedule a Callback, we will help <br> You sort any issue you facing <br> <br> 0987654321',
      buttons: [{
        text: 'Submit Request for Callback',
      
        cssClass: 'help-alert'
      },]
    });

    await alert.present();
  }

  myBookingRequiremntDetail() {
    this.navCtrl.push('MyBookingRequirementDetailPage')
  }

  goMyBookingNotRespondedDetailedMsg() {
    this.navCtrl.push('MyBookingNotRespondedDetailedMsgPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }
}
