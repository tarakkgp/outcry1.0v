import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingOngoingListingPage } from './my-booking-ongoing-listing';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyBookingOngoingListingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingOngoingListingPage),
    ComponentsModule
  ],
})
export class MyBookingOngoingListingPageModule {}
