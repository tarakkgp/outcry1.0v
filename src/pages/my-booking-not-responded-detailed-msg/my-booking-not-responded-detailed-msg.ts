import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MyBookingNotRespondedDetailedMsgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-booking-not-responded-detailed-msg',
  templateUrl: 'my-booking-not-responded-detailed-msg.html',
})
export class MyBookingNotRespondedDetailedMsgPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingNotRespondedDetailedMsgPage');
  }

  goCancelRequestBookingNotResReq() {
    this.navCtrl.push('CancelRequestMyBookingNotResReqPage');
  }


  goMyBookingOnlineListing() {
    this.navCtrl.push('MyBookingOngoingListingPage');
  }

  cancelRequestBookingResReq(){
    this.navCtrl.push('CancelRequestMyBookingResReqPage');
  }

  myBookingResProfessionalProfile() {
    this.navCtrl.push('MyBookingRespondedProfessionalProfilePage');
  }

  chatMessageList() {
    this.navCtrl.push('MessageListingPage')
  }

  
}
