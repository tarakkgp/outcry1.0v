import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingNotRespondedDetailedMsgPage } from './my-booking-not-responded-detailed-msg';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyBookingNotRespondedDetailedMsgPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingNotRespondedDetailedMsgPage),
    ComponentsModule
  ],
})
export class MyBookingNotRespondedDetailedMsgPageModule {}
