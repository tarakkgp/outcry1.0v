import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PostYourRequirementCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-your-requirement-category',
  templateUrl: 'post-your-requirement-category.html',
})
export class PostYourRequirementCategoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostYourRequirementCategoryPage');
  }

  postRequirementStarting() {
    this.navCtrl.push('QuestionnaireStartingPagePostRequirementPage');
  }

  goPostYourRequirementSegment() {
    this.navCtrl.push('PostYourRequirementSegmentPage');
  }

  professionallisting() {
    this.navCtrl.push('ProfessionallistingPage');
  }

  goQuestionnaireStartingPagePostRequirement() {
    this.navCtrl.push('QuestionnaireStartingPagePostRequirementPage');
  }
}
