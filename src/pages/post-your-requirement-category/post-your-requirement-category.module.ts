import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostYourRequirementCategoryPage } from './post-your-requirement-category';

@NgModule({
  declarations: [
    PostYourRequirementCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PostYourRequirementCategoryPage),
  ],
})
export class PostYourRequirementCategoryPageModule {}
