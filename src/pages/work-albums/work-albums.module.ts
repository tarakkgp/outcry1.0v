import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkAlbumsPage } from './work-albums';

@NgModule({
  declarations: [
    WorkAlbumsPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkAlbumsPage),
  ],
})
export class WorkAlbumsPageModule {}
