import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfirmMassageBookingProfessionalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-massage-booking-professional',
  templateUrl: 'confirm-massage-booking-professional.html',
})
export class ConfirmMassageBookingProfessionalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmMassageBookingProfessionalPage');
  }

  cancelRequest() {
    this.navCtrl.push('CancelRequestPage');
  }

  goQuestionnaireLocation() {
    this.navCtrl.push('QuestionnairelocationPage')
  }

  goQuestionaireForOtherQuestion() {
    this.navCtrl.push('QuestionaireForOtherQuestionPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }
}
