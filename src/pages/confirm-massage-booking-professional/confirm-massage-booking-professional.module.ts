import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmMassageBookingProfessionalPage } from './confirm-massage-booking-professional';

@NgModule({
  declarations: [
    ConfirmMassageBookingProfessionalPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmMassageBookingProfessionalPage),
  ],
})
export class ConfirmMassageBookingProfessionalPageModule {}
