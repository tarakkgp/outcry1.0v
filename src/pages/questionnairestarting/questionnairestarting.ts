import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnairestartingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnairestarting',
  templateUrl: 'questionnairestarting.html',
})
export class QuestionnairestartingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnairestartingPage');
  }

  questionnaireEventType() {
    this.navCtrl.push('QuestionnaireeventtypePage');
  }

  goProfessionalListing() {
    this.navCtrl.push('ProfessionalprofilePage');
  }
}
