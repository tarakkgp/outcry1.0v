import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnairestartingPage } from './questionnairestarting';

@NgModule({
  declarations: [
    QuestionnairestartingPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnairestartingPage),
  ],
})
export class QuestionnairestartingPageModule {}
