import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingRespondedProfessionalAboutPage } from './my-booking-responded-professional-about';

@NgModule({
  declarations: [
    MyBookingRespondedProfessionalAboutPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingRespondedProfessionalAboutPage),
  ],
})
export class MyBookingRespondedProfessionalAboutPageModule {}
