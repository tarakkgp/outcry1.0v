import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverviewPage } from './overview';
import { StarRatingModule } from 'ionic3-star-rating';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    OverviewPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(OverviewPage),
    SuperTabsModule,
    ComponentsModule,
  ],
})
export class OverviewPageModule {}
