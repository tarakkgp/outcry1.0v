import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireForOtherQuestionPostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-for-other-question-post-requirement',
  templateUrl: 'questionnaire-for-other-question-post-requirement.html',
})
export class QuestionnaireForOtherQuestionPostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireForOtherQuestionPostRequirementPage');
  }

  confirmMassagePostRequirement() {
    this.navCtrl.push('ConfirmMassagePostRequirementPage');
  }


}
