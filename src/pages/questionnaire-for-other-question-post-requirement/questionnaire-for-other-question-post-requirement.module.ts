import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireForOtherQuestionPostRequirementPage } from './questionnaire-for-other-question-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireForOtherQuestionPostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireForOtherQuestionPostRequirementPage),
  ],
})
export class QuestionnaireForOtherQuestionPostRequirementPageModule {}
