import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CancelRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-request',
  templateUrl: 'cancel-request.html',
})
export class CancelRequestPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelRequestPage');
  }

  postRequirementSegment() {
    this.navCtrl.push('PostYourRequirementSegmentPage');
  }

  goConfirmMessageBookingProfessional() {
    this.navCtrl.push('ConfirmMassageBookingProfessionalPage');
  }

  goConfirmMassageBookingProfessional() {
    this.navCtrl.push('ConfirmMassageBookingProfessionalPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }
}
