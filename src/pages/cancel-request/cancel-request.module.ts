import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelRequestPage } from './cancel-request';

@NgModule({
  declarations: [
    CancelRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelRequestPage),
  ],
})
export class CancelRequestPageModule {}
