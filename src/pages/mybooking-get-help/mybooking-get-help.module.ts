import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MybookingGetHelpPage } from './mybooking-get-help';

@NgModule({
  declarations: [
    MybookingGetHelpPage,
  ],
  imports: [
    IonicPageModule.forChild(MybookingGetHelpPage),
  ],
})
export class MybookingGetHelpPageModule {}
