import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-mybooking-get-help',
  templateUrl: 'mybooking-get-help.html',
})
export class MybookingGetHelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MybookingGetHelpPage');
  }
  cancel() {
    let modal = this.modalCtrl.create('Cancel');
    modal.present();
    this.viewCtrl.dismiss();
    }
    
}
