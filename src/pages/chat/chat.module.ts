import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chat } from './chat';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    Chat,
  ],
  imports: [
    IonicPageModule.forChild(Chat),
    ComponentsModule
  ],
  exports: [
    Chat
  ]
})
export class ChatModule {}
