import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireEventDurationPostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-event-duration-post-requirement',
  templateUrl: 'questionnaire-event-duration-post-requirement.html',
})
export class QuestionnaireEventDurationPostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireEventDurationPostRequirementPage');
  }

  questionnaireBudgetPostRequirement() {
    this.navCtrl.push('QuestionnaireBudgetPostRequirementPage');
  }

  goQuestionnaireEventTypePostRequirement() {
    this.navCtrl.push('QuestionnaireEventTypePostRequirementPage');
  }
}
