import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireEventDurationPostRequirementPage } from './questionnaire-event-duration-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireEventDurationPostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireEventDurationPostRequirementPage),
  ],
})
export class QuestionnaireEventDurationPostRequirementPageModule {}
