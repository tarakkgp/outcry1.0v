import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireeventdurationPage } from './questionnaireeventduration';

@NgModule({
  declarations: [
    QuestionnaireeventdurationPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireeventdurationPage),
  ],
})
export class QuestionnaireeventdurationPageModule {}
