import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireeventdurationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaireeventduration',
  templateUrl: 'questionnaireeventduration.html',
})
export class QuestionnaireeventdurationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireeventdurationPage');
  }

  questionnaireBudget() {
    this.navCtrl.push('QuestionnairebudgetPage');
  }

  goQuestionnaireEventType() {
    this.navCtrl.push('QuestionnaireeventtypePage');
  }
}
