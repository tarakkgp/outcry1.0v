import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfessionallistingPage } from './professionallisting';

@NgModule({
  declarations: [
    ProfessionallistingPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfessionallistingPage),
  ],
  exports: [
    ProfessionallistingPage
  ]
})
export class ProfessionallistingPageModule {}
