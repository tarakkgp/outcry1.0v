import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfessionallistingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-professionallisting',
  templateUrl: 'professionallisting.html',
})
export class ProfessionallistingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfessionallistingPage');
  }

  professionallistfilter() {
    this.navCtrl.push('ProfessionallistngfilterPage');
  }

  goCategory() {
    this.navCtrl.push('Service');
  }

  professionalProfile(){
    this.navCtrl.push('ProfessionalprofilePage');
  }
}
