import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireStartingPagePostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-starting-page-post-requirement',
  templateUrl: 'questionnaire-starting-page-post-requirement.html',
})
export class QuestionnaireStartingPagePostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireStartingPagePostRequirementPage');
  }

  questionnaireEventTypePostRequirement() {
    this.navCtrl.push('QuestionnaireEventTypePostRequirementPage');
  }

  goPostYourRequirementCategory() {
    this.navCtrl.push('PostYourRequirementCategoryPage');
  }
}
