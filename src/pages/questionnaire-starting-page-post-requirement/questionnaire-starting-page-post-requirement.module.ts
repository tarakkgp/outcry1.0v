import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireStartingPagePostRequirementPage } from './questionnaire-starting-page-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireStartingPagePostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireStartingPagePostRequirementPage),
  ],
})
export class QuestionnaireStartingPagePostRequirementPageModule {}
