import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfessionallistngfilterPage } from './professionallistngfilter';

@NgModule({
  declarations: [
    ProfessionallistngfilterPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfessionallistngfilterPage),
  ],
  exports: [
    ProfessionallistngfilterPage
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProfessionallistngfilterPageModule {}
