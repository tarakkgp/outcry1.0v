import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfessionallistngfilterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-professionallistngfilter',
  templateUrl: 'professionallistngfilter.html',
})
export class ProfessionallistngfilterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfessionallistngfilterPage');
  }

  professionalListingFilterResult() {
    this.navCtrl.push('ProfessionallistngfilterresultPage');
  }

  professionalListing() {
    this.navCtrl.push('ProfessionallistingPage');
  }

}
