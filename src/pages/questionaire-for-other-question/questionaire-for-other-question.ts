import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionaireForOtherQuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionaire-for-other-question',
  templateUrl: 'questionaire-for-other-question.html',
})
export class QuestionaireForOtherQuestionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionaireForOtherQuestionPage');
  }

  confirmMassageBookingProfessional() {
    this.navCtrl.push('ConfirmMassageBookingProfessionalPage');
  }
  goQuestionnaireLocation() {
    this.navCtrl.push('QuestionnairelocationPage')
  }



}
