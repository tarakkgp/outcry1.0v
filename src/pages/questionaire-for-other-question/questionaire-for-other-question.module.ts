import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionaireForOtherQuestionPage } from './questionaire-for-other-question';

@NgModule({
  declarations: [
    QuestionaireForOtherQuestionPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionaireForOtherQuestionPage),
  ],
})
export class QuestionaireForOtherQuestionPageModule {}
