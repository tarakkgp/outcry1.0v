import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MyBookingRequirementDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-booking-requirement-detail',
  templateUrl: 'my-booking-requirement-detail.html',
})
export class MyBookingRequirementDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingRequirementDetailPage');
  }

  cancelRequestBookingResReq(){
    this.navCtrl.push('CancelRequestMyBookingResReqPage');
  }

  myBookingResProfessionalProfile() {
    this.navCtrl.push('MyBookingRespondedProfessionalProfilePage');
  }

  chatMessageList() {
    this.navCtrl.push('MessageListingPage')
  }

  goMyBookingOnlineListing() {
    this.navCtrl.push('MyBookingOngoingListingPage');
  }

  gochat() {
    this.navCtrl.push('Chat');
  }

}
