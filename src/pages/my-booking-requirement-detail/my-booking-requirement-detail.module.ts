import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingRequirementDetailPage } from './my-booking-requirement-detail';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyBookingRequirementDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingRequirementDetailPage),
    ComponentsModule,
  ],
})
export class MyBookingRequirementDetailPageModule {}
