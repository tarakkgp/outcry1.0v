import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireLocationPostRequirementPage } from './questionnaire-location-post-requirement';

@NgModule({
  declarations: [
    QuestionnaireLocationPostRequirementPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireLocationPostRequirementPage),
  ],
})
export class QuestionnaireLocationPostRequirementPageModule {}
