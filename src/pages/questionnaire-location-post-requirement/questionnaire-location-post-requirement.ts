import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuestionnaireLocationPostRequirementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-questionnaire-location-post-requirement',
  templateUrl: 'questionnaire-location-post-requirement.html',
})
export class QuestionnaireLocationPostRequirementPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionnaireLocationPostRequirementPage');
  }

  confirmMassagePostRequirement() {
    this.navCtrl.push('ConfirmMassagePostRequirementPage');
  }

  goQuestionnaireDatePostRequirement() {
    this.navCtrl.push('QuestionnaireDatePostRequirementPage');
  }

  goQuestionnaireForOtherQuestionPostRequirement(){
    this.navCtrl.push('QuestionnaireForOtherQuestionPostRequirementPage');
  }
}
