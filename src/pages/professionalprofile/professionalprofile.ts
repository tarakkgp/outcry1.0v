import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { NewsPage } from '../news/news';
import { AboutPage } from '../about/about';
import { AccountPage } from '../account/account';
import { OverviewPage } from '../overview/overview';
import { WorkAlbumsPage } from '../work-albums/work-albums';
import { ReviewsPage } from '../reviews/reviews';
// import { AboutPage} from '../about/about';
/**
 * Generated class for the ProfessionalprofilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-professionalprofile',
  templateUrl: 'professionalprofile.html',
})
export class ProfessionalprofilePage {
  pages = [
    { pageName: 'AboutPage', title: 'About', icon: 'body', id: 'aboutTab'},
    { pageName: 'WorkAlbumsPage', title: 'Work Album', icon: 'help-circle', id: 'aboutTab'},
    { pageName: 'OverviewPage', title: 'Review', icon: 'flame', id: 'newsTab'},
  ];
 
  selectedTab = 0;
 
  @ViewChild(SuperTabs) superTabs: SuperTabs;
  @ViewChild(Content) content: Content;
  @ViewChild('target') target: any;
 
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) { }
 
  onTabSelect(ev: any) {
      this.selectedTab = ev.index;
      this.superTabs.clearBadge(this.pages[ev.index].id);    
  }

  questionnaireStarting() {
    this.navCtrl.push('QuestionnairestartingPage');
  }

  public scrollElement() {
    // this.content.scrollToTop(400);
    console.log("target",this.target.nativeElement)
    // this.content.scrollTo(0, this.target.nativeElement.offsetTop, 500);
  }
  scrollToBottom() {
    this.content.scrollToBottom();
  }

  goProfileListing() {
    this.navCtrl.push('ProfessionallistingPage');
  }
}
