import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfessionalprofilePage } from './professionalprofile';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    ProfessionalprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfessionalprofilePage),
    SuperTabsModule,
    ComponentsModule,
  ],
  exports: [
    ProfessionalprofilePage
  ]
})
export class ProfessionalprofilePageModule {}
