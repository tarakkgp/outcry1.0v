import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CancelRequestMyBookingNotResReqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-request-my-booking-not-res-req',
  templateUrl: 'cancel-request-my-booking-not-res-req.html',
})
export class CancelRequestMyBookingNotResReqPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelRequestMyBookingNotResReqPage');
  }

  goMyBookingRequirementDetail() {
    this.navCtrl.push('MyBookingRequirementDetailPage');
  }

  goHome() {
    this.navCtrl.push('Home');
  }

  goMyBookingOnlineListing() {
    this.navCtrl.push('MyBookingOngoingListingPage');
  }
}
