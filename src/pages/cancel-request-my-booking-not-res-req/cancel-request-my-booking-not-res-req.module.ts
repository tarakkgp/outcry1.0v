import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelRequestMyBookingNotResReqPage } from './cancel-request-my-booking-not-res-req';

@NgModule({
  declarations: [
    CancelRequestMyBookingNotResReqPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelRequestMyBookingNotResReqPage),
  ],
})
export class CancelRequestMyBookingNotResReqPageModule {}
