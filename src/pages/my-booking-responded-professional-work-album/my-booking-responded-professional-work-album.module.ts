import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingRespondedProfessionalWorkAlbumPage } from './my-booking-responded-professional-work-album';

@NgModule({
  declarations: [
    MyBookingRespondedProfessionalWorkAlbumPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingRespondedProfessionalWorkAlbumPage),
  ],
})
export class MyBookingRespondedProfessionalWorkAlbumPageModule {}
