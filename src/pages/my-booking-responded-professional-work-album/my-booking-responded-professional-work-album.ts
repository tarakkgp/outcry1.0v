import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MyBookingRespondedProfessionalWorkAlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-booking-responded-professional-work-album',
  templateUrl: 'my-booking-responded-professional-work-album.html',
})
export class MyBookingRespondedProfessionalWorkAlbumPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingRespondedProfessionalWorkAlbumPage');
  }

}
