import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingRespondedProfessionalProfilePage } from './my-booking-responded-professional-profile';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyBookingRespondedProfessionalProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingRespondedProfessionalProfilePage),
    SuperTabsModule,
    ComponentsModule
  ],
  exports: [
    MyBookingRespondedProfessionalProfilePage
  ]
})
export class MyBookingRespondedProfessionalProfilePageModule {}
