import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';
import { MyBookingRespondedProfessionalAboutPage } from './my-booking-responded-professional-about';
import { MyBookingRespondedProfessionalOverviewPage } from './my-booking-responded-professional-overview';
import { MyBookingRespondedProfessionalWorkAlbumPage } from './my-booking-responded-professional-work-album';

/**
 * Generated class for the MyBookingRespondedProfessionalProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-booking-responded-professional-profile',
  templateUrl: 'my-booking-responded-professional-profile.html',
})
export class MyBookingRespondedProfessionalProfilePage {
  pages = [
    { pageName: 'MyBookingRespondedProfessionalAboutPage', title: 'About', icon: 'body', id: 'aboutTab'},
    { pageName: 'MyBookingRespondedProfessionalWorkAlbumPage', title: 'Work Albums', icon: 'help-circle', id: 'aboutTab'},
    { pageName: 'MyBookingRespondedProfessionalOverviewPage', title: 'Review', icon: 'flame', id: 'newsTab'},
    
  ];

  selectedTab = 0;

  @ViewChild(SuperTabs) superTabs: SuperTabs;
  @ViewChild(Content) content: Content;
  @ViewChild('target') target: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  onTabSelect(ev: any) {
    this.selectedTab = ev.index;
    this.superTabs.clearBadge(this.pages[ev.index].id);
}

  public scrollElement() {
    // this.content.scrollToTop(400);
    console.log("target",this.target.nativeElement)
    // this.content.scrollTo(0, this.target.nativeElement.offsetTop, 500);
  }
  scrollToBottom() {
    this.content.scrollToBottom();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyBookingRespondedProfessionalProfilePage');
  }

  goMyBookingRequirementDetail(){
    this.navCtrl.push('MyBookingRequirementDetailPage');
  }

}
