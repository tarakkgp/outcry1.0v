import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingconfirmmessagePage } from './bookingconfirmmessage';

@NgModule({
  declarations: [
    BookingconfirmmessagePage,
  ],
  imports: [
    IonicPageModule.forChild(BookingconfirmmessagePage),
  ],
})
export class BookingconfirmmessagePageModule {}
