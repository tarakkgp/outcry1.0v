import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingcancelrequestPage } from './bookingcancelrequest';

@NgModule({
  declarations: [
    BookingcancelrequestPage,
  ],
  imports: [
    IonicPageModule.forChild(BookingcancelrequestPage),
  ],
})
export class BookingcancelrequestPageModule {}
