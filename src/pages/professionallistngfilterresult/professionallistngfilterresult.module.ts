import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfessionallistngfilterresultPage } from './professionallistngfilterresult';

@NgModule({
  declarations: [
    ProfessionallistngfilterresultPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfessionallistngfilterresultPage),
  ],
  exports: [
    ProfessionallistngfilterresultPage
  ]
})
export class ProfessionallistngfilterresultPageModule {}
