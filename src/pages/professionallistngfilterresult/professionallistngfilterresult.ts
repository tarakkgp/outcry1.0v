import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfessionallistngfilterresultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-professionallistngfilterresult',
  templateUrl: 'professionallistngfilterresult.html',
})
export class ProfessionallistngfilterresultPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfessionallistngfilterresultPage');
  }

  professionalProfile(){
    this.navCtrl.push('ProfessionalprofilePage');
  }

  goProfessionaListing() {
    this.navCtrl.push('ProfessionallistingPage');
  }

}
