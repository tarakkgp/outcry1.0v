import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PostYourRequirementSegmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-your-requirement-segment',
  templateUrl: 'post-your-requirement-segment.html',
})
export class PostYourRequirementSegmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostYourRequirementSegmentPage');
  }

  postRequirementCategory() {
    this.navCtrl.push('PostYourRequirementCategoryPage');
  }

  home() {
    this.navCtrl.push('Home');
  }
}
