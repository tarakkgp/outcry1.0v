import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostYourRequirementSegmentPage } from './post-your-requirement-segment';

@NgModule({
  declarations: [
    PostYourRequirementSegmentPage,
  ],
  imports: [
    IonicPageModule.forChild(PostYourRequirementSegmentPage),
  ],
})
export class PostYourRequirementSegmentPageModule {}
