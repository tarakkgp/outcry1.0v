import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnairedatePage } from './questionnairedate';
@NgModule({
  declarations: [
    QuestionnairedatePage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnairedatePage),
  ],
})
export class QuestionnairedatePageModule {}
