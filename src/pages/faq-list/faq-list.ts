import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FaqListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq-list',
  templateUrl: 'faq-list.html',
})
export class FaqListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqListPage');
  }

  bookService() {
    this.navCtrl.push('FaqDetailsPage');
  }

  goHelpCentreListing() {
    this.navCtrl.push('HelpCentreListingPage');
  }

}
