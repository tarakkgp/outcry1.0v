import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaqListPage } from './faq-list';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FaqListPage,
  ],
  imports: [
    IonicPageModule.forChild(FaqListPage),
    ComponentsModule
  ],
})
export class FaqListPageModule {}
