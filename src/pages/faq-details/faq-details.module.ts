import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaqDetailsPage } from './faq-details';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FaqDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(FaqDetailsPage),
    ComponentsModule
  ],
})
export class FaqDetailsPageModule {}
