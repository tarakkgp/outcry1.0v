import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingRespondedProfessionalOverviewPage } from './my-booking-responded-professional-overview';
import { StarRatingModule } from 'ionic3-star-rating';


@NgModule({
  declarations: [
    MyBookingRespondedProfessionalOverviewPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(MyBookingRespondedProfessionalOverviewPage),
  ],
})
export class MyBookingRespondedProfessionalOverviewPageModule {}
